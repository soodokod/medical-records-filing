<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;

use Illuminate\Support\Facades\Redirect;

class UnregisterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $search = $request->search;

        $patients = DB::table('vwmrfUnregistered')
                    ->where(function ($query) use ($search) {
                        $query->orwhere("PK_psDatacenter", "like", "%$search%")
                              ->orWhere("patid", "like", "%$search%")
                              ->orWhere("fullname", "like", "%$search%");
                    })
                    ->orderBy('registrydate', 'asc')
                    ->paginate(25);

        return view('unregistered.list', compact('patients', 'search'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::beginTransaction();

        try
        {
            // (start) get last index no.
            $cursor1 = DB::table('MedicalRecordsFiling')
                     ->select('Index')
                     ->limit(1)
                     ->orderBy('Index', 'desc')
                     ->first();
            // (end) get last index no.
            
            $index = explode('-', $cursor1->Index); // transform index no. "00-00-00" to array like [Pre][Mid][Post]
            
            // (start) get all unregistered patients
            $patients = DB::table('vwmrfUnregistered')
                        ->select('PK_psDatacenter','patid')
                        ->limit(500) // To solve SQLSTATE[IMSSP]: Tried to bind parameter number 2101. SQL Server supports a maximum of 2100 parameters.
                        ->orderBy('registrydate', 'asc')
                        ->get();
            // (end) get all unregistered patients

            // (start) config unregistered patients and Index 
            $data = array();

            for ($i=0; $i < count($patients); $i++) { 

                if ($index[2]++ == 99) { // increments [Post] and if equal to 99, the value will be 0
                    
                    $index[2] = 0;

                    if ($index[1]++ == 99) { // increments [Mid] and if equal to 99, the value will be 0
                        
                        $index[1] = 0;

                        $index[0]++; // increments [Pre]

                    }

                }
                
                array_push($data,
                    array(
                        'Index' => implode('-', [sprintf('%02d', $index[0]), sprintf('%02d', $index[1]), sprintf('%02d', $index[2])]), // add computed Index to Unregistered patients
                        'PK_psDatacenter' => $patients[$i]->PK_psDatacenter,
                        'patid' => $patients[$i]->patid,
                        'isActive' => true
                    )
                );

            }
            // (end) config unregistered patients and Index

            DB::table('MedicalRecordsFiling')->insert($data); // insert all unregistered patients to medical records filling

        }
        catch(\Exception $e)
        {
            DB::rollback();

            $collection = collect([ "sqlstate" => $e->getMessage() ]);

            return $collection->toJSON();
        }

        DB::commit();

        return Redirect::to('/unregistered/patients/list');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
