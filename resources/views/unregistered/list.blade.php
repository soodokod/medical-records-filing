@extends('home')

@section('list')

    <div class="tab-pane fade show active" id="registered-tab-pane" role="tabpanel"
         aria-labelledby="registered-tab">

        <div class="content-wrapper">

                <div class="content">
                    <div id="e-commerce-orders" class="page-layout carded full-width">

                        <!-- <div class="top-bg bg-primary"></div> -->

                        <!-- CONTENT -->
                        <div class="page-content">

                            <div class="page-content-card">



                            	<!-- CONTENT TOOLBAR -->
					            <div class="toolbar row no-gutters align-items-center p-4 p-sm-6">

                                    
					                <div class="col">
                                        <form method="POST" action="/register/unregistered/patients">

                                            {{ csrf_field() }}
    					                    
                                            <div class="row no-gutters align-items-center">

    					                        <!-- <div class="col-auto">

                                                    <label class="custom-control custom-checkbox">
                                                        <input type="checkbox" class="custom-control-input"/>
                                                        <span class="custom-control-indicator"></span>
                                                    </label>

                                                </div>

    					                        <div class="action-buttons col">

                                                    <div class="row no-gutters align-items-center flex-nowrap d-none d-xl-flex">

                                                        <div class="divider-vertical"></div>

                                                        <button type="button" class="btn btn-icon" aria-label="archive">
                                                            <i class="icon icon-archive"></i>
                                                        </button>

                                                        <button type="button" class="btn btn-icon" aria-label="spam">
                                                            <i class="icon icon-alert-octagon"></i>
                                                        </button>

                                                        <button type="button" class="btn btn-icon" aria-label="delete">
                                                            <i class="icon icon-delete"></i>
                                                        </button>

                                                        <div class="divider-vertical"></div>

                                                        <button type="button" class="btn btn-icon" aria-label="move to">
                                                            <i class="icon icon-folder"></i>
                                                        </button>

                                                        <button type="button" class="btn btn-icon" aria-label="labels">
                                                            <i class="icon icon-label"></i>
                                                        </button>

                                                        <button type="button" class="btn btn-icon" aria-label="move to">
                                                            <i class="icon icon-folder"></i>
                                                        </button>

                                                        <div class="divider-vertical"></div>

                                                        <button type="button" class="btn btn-icon" aria-label="more">
                                                            <i class="icon icon-dots-vertical"></i>
                                                        </button>

                                                    </div>
                                                </div> -->

                                                <button type="submit" class="btn btn-primary">
    					                        	REGISTER ALL
    					                        </button>

    					                    </div>
                                        </form>

					                </div>
                                    

					                <div class="col-auto">

					                    <div class="row no-gutters align-items-center">

					                        <!-- <span class="page-info px-2 d-none d-sm-block">1 - 100 of 980</span>

                                            <button type="button" class="btn btn-icon">
                                                <i class="icon icon-chevron-left"></i>
                                            </button>

                                            <button type="button" class="btn btn-icon">
                                                <i class="icon icon-chevron-right"></i>
                                            </button>

                                            <button type="button" class="btn btn-icon">
                                                <i class="icon icon-settings"></i>
                                            </button> -->

                                            <button type="button" class="btn btn-icon" onclick="window.location.reload(true);">
                                                <i class="icon icon-reload"></i>
                                            </button>
					                    </div>
					                </div>
					            </div>
					            <!-- / CONTENT TOOLBAR -->


                                <table id="patients-unregistered-table" class="table dataTable">

                                    <thead>
                                        <tr>

                                            <th>
                                                <div class="table-header">
                                                    <span class="column-title">Datacenter Code</span>
                                                </div>
                                            </th>

                                            <th>
                                                <div class="table-header">
                                                    <span class="column-title">Patient ID</span>
                                                </div>
                                            </th>

                                            <th>
                                                <div class="table-header">
                                                    <span class="column-title">Patient Name</span>
                                                </div>
                                            </th>

                                            <th>
                                                <div class="table-header">
                                                    <span class="column-title">Birthdate</span>
                                                </div>
                                            </th>

                                            <th>
                                                <div class="table-header">
                                                    <span class="column-title">Address</span>
                                                </div>
                                            </th>

                                        </tr>
                                    </thead>

                                    <tbody>
                                        @foreach ($patients as $patient)
                                        <tr>
                                            <td>{{ $patient->PK_psDatacenter }}</td>
                                            <td>{{ $patient->patid }}</td>
                                            <td>{{ $patient->fullname }}</td>
                                            <td>{{ $patient->UDF_birthdate }}</td>
                                            <td>{{ $patient->praddress }}</td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>





                            </div>
                        </div>

                    </div>
                </div> 








         	

    </div>

@endsection


@section('scripts')
<script type="text/javascript">

    var patients = {!! json_encode($patients) !!};

	$(document).ready(function (){

        $('#patients-unregistered-table').DataTable({
            dom : 'rtip',
            columnDefs: [
                {
                    // Target the Datacenter Code column
                    targets: 0,
                    width  : '106px',
                    orderable: false
                },
                {
                    // Target the Patient ID column
                    targets: 1,
                    orderable: false
                },
                {
                    // Target the Patient Name column
                    targets: 2,
                    orderable: false
                },
                {
                    // Target the Birthdate column
                    targets: 3,
                    orderable: false
                },
                {
                    // Target the Address column
                    targets: 4,
                    orderable: false
                },
            ],
            pageLength  : 25,
            scrollY     : 'auto',
            scrollX     : false,
            responsive  : true,
            autoWidth   : false,
            ordering    : false
        });

        $(".paginate_button.current").html(patients.current_page);

        $(".dataTables_info").html("Showing " + (patients.total ? patients.from : 0) + " to " + (patients.total ? patients.to : 0) + " of " + patients.total + " entries");
        
        /** (start) Previous **/
        if(patients.prev_page_url){

            $("#patients-unregistered-table_previous")
                .removeClass("disabled")
                .bind( "click", function() {
                    window.location.href = patients.prev_page_url;
                });
        }
        /** (end) Previous **/


        /** (start) Next **/
        if(patients.next_page_url){

            $("#patients-unregistered-table_next")
                .removeClass("disabled")
                .bind( "click", function() {
                    window.location.href = patients.next_page_url;
                });
        }
        /** (end) Next **/
        
    });
</script>
@endsection