@extends('home')

@section('list')

    <div class="tab-pane fade show active" id="registered" role="tabpanel"
         aria-labelledby="registered-tab" style="padding-bottom: 0;">


         	<div class="content-wrapper">

                <div class="content">
                    <div id="e-commerce-orders" class="page-layout carded full-width">

                        <!-- <div class="top-bg bg-primary"></div> -->

                        <!-- CONTENT -->
                        <div class="page-content">

                            <div class="page-content-card">

                                <table id="patients-registered-table" class="table dataTable">

                                    <thead>
                                        <tr>

                                            <th>
                                                <div class="table-header">
                                                    <span class="column-title">Index</span>
                                                </div>
                                            </th>

                                            <th>
                                                <div class="table-header">
                                                    <span class="column-title">Datacenter Code</span>
                                                </div>
                                            </th>

                                            <th>
                                                <div class="table-header">
                                                    <span class="column-title">Patient ID</span>
                                                </div>
                                            </th>

                                            <th>
                                                <div class="table-header">
                                                    <span class="column-title">Patient Name</span>
                                                </div>
                                            </th>

                                            <th>
                                                <div class="table-header">
                                                    <span class="column-title">Birthdate</span>
                                                </div>
                                            </th>

                                            <th>
                                                <div class="table-header">
                                                    <span class="column-title">Address</span>
                                                </div>
                                            </th>

                                        </tr>
                                    </thead>

                                    <tbody>
                                        @foreach ($patients as $patient)
                                        <tr>
                                            <td>{{ $patient->Index }}</td>
                                            <td>{{ $patient->PK_psDatacenter }}</td>
                                            <td>{{ $patient->patid }}</td>
                                            <td>{{ $patient->fullname }}</td>
                                            <td>{{ $patient->birthdate }}</td>
                                            <td>{{ $patient->praddress }}</td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>

                                
                            </div>
                        </div>
                        <!-- / CONTENT -->
                    </div>

                    
                </div>

            </div>

    </div>
@endsection


@section('scripts')
<script type="text/javascript">

    var patients = {!! json_encode($patients) !!};

	$(document).ready(function (){

        $('#patients-registered-table').DataTable({
            dom : 'rtip',
            columnDefs: [
                {
                    // Target the Index column
                    targets: 0,
                    width  : '72px',
                    orderable: false
                },
                {
                    // Target the Datacenter Code column
                    targets: 1,
                    orderable: false
                },
                {
                    // Target the Patient ID column
                    targets: 2,
                    orderable: false
                },
                {
                    // Target the Patient Name column
                    targets: 3,
                    orderable: false
                },
                {
                    // Target the Birthdate column
                    targets: 4,
                    orderable: false
                },
                {
                    // Target the Address column
                    targets: 5,
                    orderable: false
                },
            ],
            pageLength  : 25,
            scrollY     : 'auto',
            scrollX     : false,
            responsive  : true,
            autoWidth   : false,
            order       : [
                              [ 0, 'desc' ] // Index
                          ]
        });

        $(".paginate_button.current").html(patients.current_page);

        $(".dataTables_info").html("Showing " + (patients.total ? patients.from : 0) + " to " + (patients.total ? patients.to : 0) + " of " + patients.total + " entries");
        
        /** (start) Previous **/
        if(patients.prev_page_url){

            $("#patients-registered-table_previous")
                .removeClass("disabled")
                .bind( "click", function() {
                    window.location.href = patients.prev_page_url;
                });
        }
        /** (end) Previous **/


        /** (start) Next **/
        if(patients.next_page_url){

            $("#patients-registered-table_next")
                .removeClass("disabled")
                .bind( "click", function() {
                    window.location.href = patients.next_page_url;
                });
        }
        /** (end) Next **/
        
    });
</script>
@endsection