<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>

        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name') }}</title>

        <!-- STYLESHEETS -->
        <style type="text/css">
            [fuse-cloak],
            .fuse-cloak {
                display: none !important;
            }
        </style>

        <!-- Icons.css -->
        <link type="text/css" rel="stylesheet" href="{{ asset('library/Fuse/assets/icons/fuse-icon-font/style.css') }}">

        <!-- Animate.css -->
        <link type="text/css" rel="stylesheet" href="{{ asset('library/Fuse/assets/vendor/animate.css/animate.min.css') }}">

        <!-- PNotify -->
        <link type="text/css" rel="stylesheet" href="{{ asset('library/Fuse/assets/vendor/pnotify/pnotify.custom.min.css') }}">

        <!-- Nvd3 - D3 Charts -->
        <link type="text/css" rel="stylesheet" href="{{ asset('library/Fuse/assets/vendor/nvd3/build/nv.d3.min.css') }}"/>

        <!-- Perfect Scrollbar -->
        <link type="text/css" rel="stylesheet" href="{{ asset('library/Fuse/assets/vendor/perfect-scrollbar/css/perfect-scrollbar.min.css') }}"/>

        <!-- Fuse Html -->
        <link type="text/css" rel="stylesheet" href="{{ asset('library/Fuse/assets/vendor/fuse-html/fuse-html.min.css') }}"/>

        <!-- Main CSS -->
        <link type="text/css" rel="stylesheet" href="{{ asset('library/Fuse/assets/css/main.css') }}">
        <!-- / STYLESHEETS -->

        <!-- JAVASCRIPT -->

        <!-- jQuery -->
        <script type="text/javascript" src="{{ asset('library/Fuse/assets/vendor/jquery/dist/jquery.min.js') }}"></script>

        <!-- Mobile Detect -->
        <script type="text/javascript" src="{{ asset('library/Fuse/assets/vendor/mobile-detect/mobile-detect.min.js') }}"></script>

        <!-- Perfect Scrollbar -->
        <script type="text/javascript"
                src="{{ asset('library/Fuse/assets/vendor/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js') }}"></script>

        <!-- Popper.js -->
        <script type="text/javascript" src="{{ asset('library/Fuse/assets/vendor/popper.js/index.js') }}"></script>

        <!-- Bootstrap -->
        <script type="text/javascript" src="{{ asset('library/Fuse/assets/vendor/bootstrap/bootstrap.min.js') }}"></script>

        <!-- Nvd3 - D3 Charts -->
        <script type="text/javascript" src="{{ asset('library/Fuse/assets/vendor/d3/d3.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('library/Fuse/assets/vendor/nvd3/build/nv.d3.min.js') }}"></script>

        <!-- Data tables -->
        <script type="text/javascript" src="{{ asset('library/Fuse/assets/vendor/datatables.net/js/jquery.dataTables.min.js') }}"></script>

        <script type="text/javascript"
                src="{{ asset('library/Fuse/assets/vendor/datatables-responsive/js/dataTables.responsive.js') }}"></script>

        <!-- PNotify -->
        <script type="text/javascript" src="{{ asset('library/Fuse/assets/vendor/pnotify/pnotify.custom.min.js') }}"></script>

        <!-- Fuse Html -->
        <script type="text/javascript" src="{{ asset('library/Fuse/assets/vendor/fuse-html/fuse-html.min.js') }}"></script>

        <!-- Main JS -->
        <script type="text/javascript" src="{{ asset('library/Fuse/assets/js/main.js') }}"></script>

        <!-- Vue JS -->
        <script type="text/javascript" src="{{ asset('library/Vue.js/vue.js') }}"></script>

        <!-- / JAVASCRIPT -->
    </head>

    <body class="layout layout-vertical layout-left-navigation layout-above-toolbar">

        <div id="wrapper">

            <div class="content-wrapper">

                <div class="content" style="margin-top: 0;">
                    <div id="project-dashboard" class="page-layout simple right-sidebar tabbed mail">

                        <div class="page-content-wrapper">

                            <!-- HEADER -->
                            <div
                                class="page-header bg-primary text-auto d-flex flex-column justify-content-between px-6 pt-4 pb-0">

                                <div class="row no-gutters align-items-start justify-content-between flex-nowrap">

                                    <div>
                                        <span class="h2">BizBox Hospital Information System</span>
                                    </div>

                                    <!-- <button type="button" class="sidebar-toggle-button btn btn-icon d-block d-xl-none"
                                            data-fuse-bar-toggle="dashboard-project-sidebar"
                                            aria-label="Toggle sidebar">
                                        <i class="icon icon-menu"></i>
                                    </button> -->
                                </div>

                                <form>
                                    <div class="search-bar row align-items-center no-gutters bg-white text-auto">

                                        <!-- <button type="button"
                                                class="sidebar-toggle-button btn btn-icon d-block d-lg-none"
                                                data-fuse-bar-toggle="mail-sidebar">
                                            <i class="icon icon-menu"></i>
                                        </button> -->

                                        <i class="icon-magnify s-6 mx-4"></i>

                                        <input class="search-bar-input col" name="search" type="text" placeholder="Search for an patient's name or index" value="{{$search}}">

                                    </div>
                                </form>
                                
                                <div class="row no-gutters align-items-center project-selection">

                                    <div class="selected-project h6 px-4 py-2">{{ config('app.name') }} App</div>

                                    <!-- <div class="project-selector">
                                        <button type="button" class="btn btn-icon">
                                            <i class="icon icon-dots-horizontal"></i>
                                        </button>
                                    </div> -->

                                </div>

                            </div>
                            <!-- / HEADER -->

                            <!-- CONTENT -->
                            @yield('content')
                            <!-- / CONTENT -->

                        </div>

                    </div>

                    <script type="text/javascript" src="{{ asset('library/Fuse/assets/js/apps/dashboard/project.js') }}"></script>

                </div>

            </div>

        </div>

        @yield('scripts')

    </body>

</html>