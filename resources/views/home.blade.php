@extends('layouts.app')

@section('content')
<div class="page-content">

    <ul class="nav nav-tabs" id="myTab" role="tablist">

        <li class="nav-item">
            <!-- <a class="nav-link btn active" id="registered-tab" data-toggle="tab"
               href="#registered-tab-pane" role="tab"
               aria-controls="registered-tab-pane" aria-expanded="true">Registered</a> -->
               <a class="nav-link btn @if(starts_with(Request::path(), 'registered')) active @endif" id="registered-tab" 
               href="/registered/patients/list" role="tab"
               aria-controls="/patients/unregistered/list" aria-expanded="true">Registered</a>
        </li>

        <li class="nav-item">
            <!-- <a class="nav-link btn" id="unregistered-tab" data-toggle="tab"
               href="#unregistered-tab-pane"
               role="tab" aria-controls="unregistered-tab-pane">Unregistered</a> -->
               <a class="nav-link btn @if(starts_with(Request::path(), 'unregistered')) active @endif" id="unregistered-tab" 
               href="/unregistered/patients/list"
               role="tab" aria-controls="unregistered-tab-pane">Unregistered</a>
        </li>

    </ul>

    <div class="tab-content">
        @yield('list')
    </div>

</div>
@endsection
